package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.annotations.HasMany;
import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Media type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Media", authRules = {
  @AuthRule(allow = AuthStrategy.PRIVATE, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ })
})
public final class Media implements Model {
  public static final QueryField ID = field("Media", "id");
  public static final QueryField TYPE = field("Media", "type");
  public static final QueryField URL = field("Media", "url");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="MediaType") MediaType type;
  private final @ModelField(targetType="AWSURL") String url;
  private final @ModelField(targetType="EventMedia") @HasMany(associatedWith = "media", type = EventMedia.class) List<EventMedia> events = null;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public MediaType getType() {
      return type;
  }
  
  public String getUrl() {
      return url;
  }
  
  public List<EventMedia> getEvents() {
      return events;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private Media(String id, MediaType type, String url) {
    this.id = id;
    this.type = type;
    this.url = url;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Media media = (Media) obj;
      return ObjectsCompat.equals(getId(), media.getId()) &&
              ObjectsCompat.equals(getType(), media.getType()) &&
              ObjectsCompat.equals(getUrl(), media.getUrl()) &&
              ObjectsCompat.equals(getCreatedAt(), media.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), media.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getType())
      .append(getUrl())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Media {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("type=" + String.valueOf(getType()) + ", ")
      .append("url=" + String.valueOf(getUrl()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static BuildStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static Media justId(String id) {
    return new Media(
      id,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      type,
      url);
  }
  public interface BuildStep {
    Media build();
    BuildStep id(String id);
    BuildStep type(MediaType type);
    BuildStep url(String url);
  }
  

  public static class Builder implements BuildStep {
    private String id;
    private MediaType type;
    private String url;
    @Override
     public Media build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Media(
          id,
          type,
          url);
    }
    
    @Override
     public BuildStep type(MediaType type) {
        this.type = type;
        return this;
    }
    
    @Override
     public BuildStep url(String url) {
        this.url = url;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, MediaType type, String url) {
      super.id(id);
      super.type(type)
        .url(url);
    }
    
    @Override
     public CopyOfBuilder type(MediaType type) {
      return (CopyOfBuilder) super.type(type);
    }
    
    @Override
     public CopyOfBuilder url(String url) {
      return (CopyOfBuilder) super.url(url);
    }
  }
  
}
