package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.AuthStrategy;
import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.ModelOperation;
import com.amplifyframework.core.model.annotations.AuthRule;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Task type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Tasks", authRules = {
  @AuthRule(allow = AuthStrategy.PRIVATE, operations = { ModelOperation.CREATE, ModelOperation.UPDATE, ModelOperation.DELETE, ModelOperation.READ }),
  @AuthRule(allow = AuthStrategy.OWNER, ownerField = "owner", identityClaim = "cognito:username", provider = "userPools", operations = { ModelOperation.CREATE, ModelOperation.DELETE })
})
@Index(name = "byUser", fields = {"userID"})
public final class Task implements Model {
  public static final QueryField ID = field("Task", "id");
  public static final QueryField NAME = field("Task", "name");
  public static final QueryField DESCRIPTION = field("Task", "description");
  public static final QueryField TASK_TYPE = field("Task", "taskType");
  public static final QueryField USER_ID = field("Task", "userID");
  public static final QueryField BEGIN_DATE = field("Task", "beginDate");
  public static final QueryField DUE_DATE = field("Task", "dueDate");
  public static final QueryField FINISHED_DATE = field("Task", "finishedDate");
  public static final QueryField IS_COMPLETED = field("Task", "isCompleted");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="String") String name;
  private final @ModelField(targetType="String") String description;
  private final @ModelField(targetType="TaskType") TaskType taskType;
  private final @ModelField(targetType="ID") String userID;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime beginDate;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime dueDate;
  private final @ModelField(targetType="AWSDateTime") Temporal.DateTime finishedDate;
  private final @ModelField(targetType="Boolean") Boolean isCompleted;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public String getName() {
      return name;
  }
  
  public String getDescription() {
      return description;
  }
  
  public TaskType getTaskType() {
      return taskType;
  }
  
  public String getUserId() {
      return userID;
  }
  
  public Temporal.DateTime getBeginDate() {
      return beginDate;
  }
  
  public Temporal.DateTime getDueDate() {
      return dueDate;
  }
  
  public Temporal.DateTime getFinishedDate() {
      return finishedDate;
  }
  
  public Boolean getIsCompleted() {
      return isCompleted;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private Task(String id, String name, String description, TaskType taskType, String userID, Temporal.DateTime beginDate, Temporal.DateTime dueDate, Temporal.DateTime finishedDate, Boolean isCompleted) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.taskType = taskType;
    this.userID = userID;
    this.beginDate = beginDate;
    this.dueDate = dueDate;
    this.finishedDate = finishedDate;
    this.isCompleted = isCompleted;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Task task = (Task) obj;
      return ObjectsCompat.equals(getId(), task.getId()) &&
              ObjectsCompat.equals(getName(), task.getName()) &&
              ObjectsCompat.equals(getDescription(), task.getDescription()) &&
              ObjectsCompat.equals(getTaskType(), task.getTaskType()) &&
              ObjectsCompat.equals(getUserId(), task.getUserId()) &&
              ObjectsCompat.equals(getBeginDate(), task.getBeginDate()) &&
              ObjectsCompat.equals(getDueDate(), task.getDueDate()) &&
              ObjectsCompat.equals(getFinishedDate(), task.getFinishedDate()) &&
              ObjectsCompat.equals(getIsCompleted(), task.getIsCompleted()) &&
              ObjectsCompat.equals(getCreatedAt(), task.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), task.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getName())
      .append(getDescription())
      .append(getTaskType())
      .append(getUserId())
      .append(getBeginDate())
      .append(getDueDate())
      .append(getFinishedDate())
      .append(getIsCompleted())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Task {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("name=" + String.valueOf(getName()) + ", ")
      .append("description=" + String.valueOf(getDescription()) + ", ")
      .append("taskType=" + String.valueOf(getTaskType()) + ", ")
      .append("userID=" + String.valueOf(getUserId()) + ", ")
      .append("beginDate=" + String.valueOf(getBeginDate()) + ", ")
      .append("dueDate=" + String.valueOf(getDueDate()) + ", ")
      .append("finishedDate=" + String.valueOf(getFinishedDate()) + ", ")
      .append("isCompleted=" + String.valueOf(getIsCompleted()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static BuildStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static Task justId(String id) {
    return new Task(
      id,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      name,
      description,
      taskType,
      userID,
      beginDate,
      dueDate,
      finishedDate,
      isCompleted);
  }
  public interface BuildStep {
    Task build();
    BuildStep id(String id);
    BuildStep name(String name);
    BuildStep description(String description);
    BuildStep taskType(TaskType taskType);
    BuildStep userId(String userId);
    BuildStep beginDate(Temporal.DateTime beginDate);
    BuildStep dueDate(Temporal.DateTime dueDate);
    BuildStep finishedDate(Temporal.DateTime finishedDate);
    BuildStep isCompleted(Boolean isCompleted);
  }
  

  public static class Builder implements BuildStep {
    private String id;
    private String name;
    private String description;
    private TaskType taskType;
    private String userID;
    private Temporal.DateTime beginDate;
    private Temporal.DateTime dueDate;
    private Temporal.DateTime finishedDate;
    private Boolean isCompleted;
    @Override
     public Task build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Task(
          id,
          name,
          description,
          taskType,
          userID,
          beginDate,
          dueDate,
          finishedDate,
          isCompleted);
    }
    
    @Override
     public BuildStep name(String name) {
        this.name = name;
        return this;
    }
    
    @Override
     public BuildStep description(String description) {
        this.description = description;
        return this;
    }
    
    @Override
     public BuildStep taskType(TaskType taskType) {
        this.taskType = taskType;
        return this;
    }
    
    @Override
     public BuildStep userId(String userId) {
        this.userID = userId;
        return this;
    }
    
    @Override
     public BuildStep beginDate(Temporal.DateTime beginDate) {
        this.beginDate = beginDate;
        return this;
    }
    
    @Override
     public BuildStep dueDate(Temporal.DateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }
    
    @Override
     public BuildStep finishedDate(Temporal.DateTime finishedDate) {
        this.finishedDate = finishedDate;
        return this;
    }
    
    @Override
     public BuildStep isCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String name, String description, TaskType taskType, String userId, Temporal.DateTime beginDate, Temporal.DateTime dueDate, Temporal.DateTime finishedDate, Boolean isCompleted) {
      super.id(id);
      super.name(name)
        .description(description)
        .taskType(taskType)
        .userId(userId)
        .beginDate(beginDate)
        .dueDate(dueDate)
        .finishedDate(finishedDate)
        .isCompleted(isCompleted);
    }
    
    @Override
     public CopyOfBuilder name(String name) {
      return (CopyOfBuilder) super.name(name);
    }
    
    @Override
     public CopyOfBuilder description(String description) {
      return (CopyOfBuilder) super.description(description);
    }
    
    @Override
     public CopyOfBuilder taskType(TaskType taskType) {
      return (CopyOfBuilder) super.taskType(taskType);
    }
    
    @Override
     public CopyOfBuilder userId(String userId) {
      return (CopyOfBuilder) super.userId(userId);
    }
    
    @Override
     public CopyOfBuilder beginDate(Temporal.DateTime beginDate) {
      return (CopyOfBuilder) super.beginDate(beginDate);
    }
    
    @Override
     public CopyOfBuilder dueDate(Temporal.DateTime dueDate) {
      return (CopyOfBuilder) super.dueDate(dueDate);
    }
    
    @Override
     public CopyOfBuilder finishedDate(Temporal.DateTime finishedDate) {
      return (CopyOfBuilder) super.finishedDate(finishedDate);
    }
    
    @Override
     public CopyOfBuilder isCompleted(Boolean isCompleted) {
      return (CopyOfBuilder) super.isCompleted(isCompleted);
    }
  }
  
}
