package com.example.androidkotlin.data.events

import android.util.Log
import com.amplifyframework.api.ApiException
import com.amplifyframework.api.rest.RestOptions
import com.amplifyframework.core.model.query.Where
import com.amplifyframework.datastore.DataStoreChannelEventName
import com.amplifyframework.datastore.generated.model.Event
import com.amplifyframework.hub.HubChannel
import kotlin.collections.ArrayList
import com.example.androidkotlin.data.Result
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.lang.Exception
import com.amplifyframework.kotlin.core.Amplify as KotlinAmplify

class EventsDataSource() {

    var ready = false

    suspend fun getEvents(): Result<List<Event>> {
        try {
            var eventsList = ArrayList<Event>()
            var error: Result.Error? = null

            if (!ready) {
                checkIfDatastoreIsReady()
            }

            KotlinAmplify.DataStore.query(Event::class)
                .catch {
                    Log.e("QueryError", "Query failed", it)
                    error = Result.Error(Exception(it.message))
                }
                .collect {
                    eventsList.add(it)
                    Log.i("EventQuery", "Title: ${it.name}")
                }

            return if (error != null) {
                error!!
            } else {
                Result.Success(eventsList)
            }

        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            return Result.Error(e)
        }
    }

    suspend fun getEvent(pId: String): Result<Event> {
        try {
            var event: Event = Event.builder().build()
            var error: Result.Error? = null
            KotlinAmplify.DataStore.query(Event::class, Where.id(pId))
                .catch {
                    error = Result.Error(Exception(it.message))
                    Log.e("MyAmplifyApp/getEvent", "Query failed", it)
                }.collect {
                    event = it;
                    Log.i("MyAmplifyApp/getEvent", "Title: ${it.name}")
                }
            if (error != null) {
                error!!.exception.message?.let { Log.i("MyAmplifyApp/getEvent", it) }
                return error as Result.Error
            }
            return Result.Success(event)
        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp/getEvent", it)
            }
            return Result.Error(e)
        }
    }

    private suspend fun checkIfDatastoreIsReady() {
        try {
            coroutineScope {
                KotlinAmplify.Hub.subscribe(HubChannel.DATASTORE)
                { it.name == DataStoreChannelEventName.SYNC_QUERIES_READY.toString() }
                    .collect {
                        ready = true
                        Log.i("HubEvent", "Kotlin datastore is available to do query")
                        this.cancel()
                    }
            }
        } catch (e: CancellationException) {
            Log.i("CancellationOfFlow", "Flow has to be cancelled")
        }
    }

    suspend fun takeAttendance(
        pLat: String,
        pLong: String,
        qrCode: String,
        userId: String
    ):Result<String> {
        val strRequest =
            "{\'userID\':\'$userId\', \'qrData\':\'$qrCode\', \'userLat\':\'${pLat}\', \'userLng\':\'${pLong}\'}"
        val request = RestOptions.builder()
            .addPath("/take-attendance")
            .addBody(strRequest.toByteArray())
            .build()
        return try {
            val response = KotlinAmplify.API.post(
                request
            )
            Log.i("MyAmplifyApp", "POST succeeded: $response.")
            Result.Success(String(response.data.rawBytes))
        } catch (error: ApiException) {
            Log.e("MyAmplifyApp", "GET failed", error)
            Result.Success(error.message.toString())
        }
    }
}