package com.example.androidkotlin.ui.events

import java.util.*

data class EventsView (
        val id: String,
        val name: String,
        val description: String
) {
        override fun toString() = "Evento - Nombre: $name, id: $id"
}