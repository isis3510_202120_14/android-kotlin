package com.example.androidkotlin.ui.tasks

class TaskResult (
    val success: TaskUserView? = null,
    val error: String? = null
)