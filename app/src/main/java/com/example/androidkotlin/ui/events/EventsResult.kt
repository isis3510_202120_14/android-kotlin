package com.example.androidkotlin.ui.events

import com.amplifyframework.datastore.generated.model.Event

data class EventsResult (
    val success: List<Event>? = null,
    val error: String? = null
)