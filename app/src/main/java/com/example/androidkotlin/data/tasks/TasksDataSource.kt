package com.example.androidkotlin.data.tasks

import android.util.Log
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.model.query.Where
import com.amplifyframework.core.model.temporal.Temporal
import com.amplifyframework.datastore.DataStoreChannelEventName
import com.amplifyframework.datastore.generated.model.Task
import com.amplifyframework.hub.HubChannel
import com.amplifyframework.kotlin.core.Amplify as KotlinAmplify
import com.example.androidkotlin.data.Result
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import java.lang.Exception
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class TasksDataSource {
    var ready = false
    val df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'")

    suspend fun getTasks(): Result<List<Task>> {
        try {
            val tasksList = ArrayList<Task>()
            var error: Result.Error? = null

            if (!ready){
                checkIfDatastoreIsReady()
            }

            KotlinAmplify.DataStore.query(Task::class, Where.matches(Task.IS_COMPLETED.eq(false)
                .and(Task.USER_ID.eq(Amplify.Auth.currentUser.userId))))
                .catch {
                    Log.e("QueryError", "Query failed", it)
                    error = Result.Error(Exception(it.message))
                }
                .collect {
                    tasksList.add(it)
                    Log.i("EventQuery", "Title: ${it.name}")
                }

            return if (error != null ){
                error!!
            } else{
                Result.Success(tasksList as List<Task>)
            }

        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            return Result.Error(e)
        }
    }

    suspend fun getTask(id: String): Result<Task> {
        try {
            var task: Task = Task.builder().build()
            var error: Result.Error? = null
            KotlinAmplify.DataStore.query(Task::class, Where.id(id))
                .catch {
                    error = Result.Error(Exception(it.message))
                    Log.e("MyAmplifyApp/getEvent", "Query failed", it)
                }.collect {
                    task = it;
                    Log.i("MyAmplifyApp/getEvent", "Title: ${it.name}")
                }
            if (error != null) {
                error!!.exception.message?.let { Log.i("MyAmplifyApp/getEvent", it) }
                return error as Result.Error
            }
            return Result.Success(task)
        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp/getEvent", it)
            }
            return Result.Error(e)
        }
    }

    suspend fun deleteTask(id: String): Result<Task> {
        try {
            var task: Task = Task.builder().build()
            var error: Result.Error? = null

            KotlinAmplify.DataStore.query(Task::class, Where.id(id))
                .catch {
                    error = Result.Error(Exception(it.message))
                    Log.e("MyAmplifyApp", "Query failed", it)
                }
                .onEach {
                    task  = it
                    KotlinAmplify.DataStore.delete(it)
                }
                .catch {
                    error = Result.Error(Exception(it.message))
                    Log.e("MyAmplifyApp", "Delete failed", it)
                }
                .collect {
                    task = it
                    Log.i("MyAmplifyApp", "Deleted a task")
                }

            if (error != null) {
                error!!.exception.message?.let { Log.i("MyAmplifyApp", it) }
                return error as Result.Error
            }
            return Result.Success(task)
        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            return Result.Error(e)
        }
    }

    suspend fun createTask(task: Task): Result<Task> {
        val createTask = task.copyOfBuilder()
            .id(null)
            .userId(Amplify.Auth.currentUser.userId)
            .beginDate(Temporal.DateTime(LocalDateTime.now().format(df)))
            .build()
        return try {
            KotlinAmplify.DataStore.save(createTask)
            Log.i("MyAmplifyApp", "Saved a post.")
            Result.Success(task)

        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            Result.Error(e)
        }
    }

    suspend fun updateTask(id: String, task: Task): Result<Task> {
        val updateTask = task.copyOfBuilder().id(id).build()
        return try {
            KotlinAmplify.DataStore.save(updateTask)
            Log.i("MyAmplifyApp", "Updated a post.")
            Result.Success(updateTask)

        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            Result.Error(e)
        }
    }

    suspend  fun completeTask(id: String, task: Task): Result<Task> {
        val updateTask = task.copyOfBuilder()
            .id(id)
            .isCompleted(true)
            .finishedDate(Temporal.DateTime(LocalDateTime.now().format(df)))
            .build()
        return try {
            KotlinAmplify.DataStore.save(updateTask)
            Log.i("MyAmplifyApp", "Updated a post.")
            Result.Success(updateTask)

        } catch (e: Exception) {
            e.message?.let {
                Log.i("MyAmplifyApp", it)
            }
            Result.Error(e)
        }
    }

    private suspend fun checkIfDatastoreIsReady(){
        try {
            coroutineScope {
                KotlinAmplify.Hub.subscribe(HubChannel.DATASTORE)
                { it.name == DataStoreChannelEventName.SYNC_QUERIES_READY.toString() }
                    .collect {
                        ready = true
                        Log.i("HubEvent", "Kotlin datastore is available to do query")
                        this.cancel()
                    }
            }
        }
        catch ( e: CancellationException){
            Log.i("CancellationOfFlow", "Flow has to be cancelled")
        }
    }
}