package com.example.androidkotlin.ui.events

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlin.data.events.EventsRepository

class EventsViewModelFactory(private val eventsRepository: EventsRepository):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return EventsViewModel(eventsRepository) as T
    }
}