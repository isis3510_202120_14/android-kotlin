package com.example.androidkotlin.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.amplifyframework.datastore.generated.model.Event
import com.example.androidkotlin.databinding.EventItemBinding
import com.example.androidkotlin.ui.home.HomeFragmentDirections

class EventsAdapter(val events: ArrayList<Event>) :
    RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    class ViewHolder(val eventItemBinding: EventItemBinding) :
        RecyclerView.ViewHolder(eventItemBinding.root) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val eventItemBinding = EventItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(eventItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = events[position]
        holder.eventItemBinding.event = event
        holder.itemView.setOnClickListener {
            val navController = Navigation.findNavController(holder.itemView)
            navController.navigate(HomeFragmentDirections.actionHomeFragmentToEventDetailFragment(event.id))
        }
    }

    override fun getItemCount(): Int {
        return events.size
    }
}