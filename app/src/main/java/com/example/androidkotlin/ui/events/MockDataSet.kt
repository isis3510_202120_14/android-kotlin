package com.example.androidkotlin.ui.events

object MockDataSet {
    val one = listOf(
        "https://images.unsplash.com/photo-1513072064285-240f87fa81e8?w=1080",
        "https://images.unsplash.com/photo-1540866225557-9e4c58100c67?w=1080",
        "https://images.unsplash.com/photo-1573322867455-fc97c490c14c?w=1080",
        "https://images.unsplash.com/photo-1519817650390-64a93db51149?w=1080",
        "https://images.unsplash.com/photo-1581357825340-32259110788a?w=1080",
        "https://images.unsplash.com/photo-1581441117193-63e8f6547081?w=1080",
        "https://images.unsplash.com/photo-1542273917363-3b1817f69a2d?w=1080",
    )
}