import json
import boto3
import datetime
import ast
import uuid
from boto3.dynamodb.conditions import Key, Attr
from geopy import distance

client = boto3.client('dynamodb', region_name="us-east-1")

ATTENDANCE_TABLE = "Attendance-i52hyaiobfeqdpntkhsqm7wxya-dev"
EVENT_TABLE = "Event-i52hyaiobfeqdpntkhsqm7wxya-dev"

dynamo = boto3.resource('dynamodb', region_name="us-east-1")
event_table = dynamo.Table(EVENT_TABLE)


def calculate_distance(event_location, user_location):
    ''''
    Calculates the distance between two {lat, lng} points.
    The distance is measured in meters.
    '''
    coords1 = (event_location["lat"], event_location["lng"])
    coords2 = (user_location["lat"], user_location["lng"])

    return distance.geodesic(coords1, coords2).meters


def get_attendance_object(event_id, user_id):
    data = client.query(
        TableName=ATTENDANCE_TABLE,
        IndexName='byEvent',
        KeyConditionExpression="eventID = :eventId",
        FilterExpression="userID = :userId",
        ExpressionAttributeValues={
            ":eventId": {"S": event_id},
            ":userId": {"S": user_id},
        }
    )
    print("Attendance object:", data)
    if len(data["Items"]) > 0:
        return data["Items"][0]
    else:
        return None


def create_attendance_object(event_id, user_id):
    data = client.put_item(
        TableName=ATTENDANCE_TABLE,
        Item={
            'id': {'S': str(uuid.uuid4())},
            '__typename': {'S': "Attendance"},
            '_lastChangedAt': {'N': str(int(datetime.datetime.utcnow().timestamp() * 1000))},
            '_version': {'N': "1"},
            'attendedDateTime': {'NULL': True},
            'updatedAt': {'S': datetime.datetime.utcnow().isoformat()[:-3] + "Z"},
            'createdAt': {'S': datetime.datetime.utcnow().isoformat()[:-3] + "Z"},
            'userID': {'S': user_id},
            'eventID': {'S': event_id},
            'attended': {'BOOL': False},
            'mandatoryEvent': {'BOOL': False},
        }
    )
    print("CREATE ATTENDANCE:", data)


def get_event_info(event_id):
    data = event_table.get_item(
        Key={
            'id': event_id
        },
        AttributesToGet=[
            'isPublic',
            'latitude',
            'longitude',
        ],
    )
    print("GET EVENT LOC: ", data.get("Item", None), type(data))
    return data["Item"]['isPublic'], {
        "lat": data["Item"]['latitude'],
        "lng": data["Item"]['longitude'],
    }


def attend_attendance(attendance):
    response = client.update_item(
        TableName=ATTENDANCE_TABLE,
        Key={
            'id': attendance["id"],
        },
        AttributeUpdates={
            'attended': {'Value': {'BOOL': True}},
            'attendedDateTime': {'Value': {'S': datetime.datetime.utcnow().isoformat()[:-10] + "Z"}},
        },
        ReturnValues="UPDATED_NEW"
    )
    print("Update object:", response)
    return response


def get_event_id(qr_data):
    data_parts = str(qr_data).split("TECHO_APP_ATTDCE_E-")
    return data_parts[1]


def handler(event, context):
    print('received event:')
    print(event)
    event = ast.literal_eval(event["body"])
    print(event)

    user_id = event.get("userID", None)
    qr_data = event.get("qrData", None)
    user_lat = event.get("userLat", None)
    user_lng = event.get("userLng", None)

    statusCode = 400
    message = "No se logró tomar la asistencia"

    try:
        event_id = get_event_id(qr_data=qr_data)
        public_event, event_location = get_event_info(event_id=event_id)
        try:
            attendance = get_attendance_object(
                event_id=event_id, user_id=user_id)
            if attendance == None and public_event == True:
                create_attendance_object(
                    event_id=event_id, user_id=user_id)
                attendance = get_attendance_object(
                    event_id=event_id, user_id=user_id)
            elif attendance == None:
                raise Exception(
                    'El evento no es público y el usuario no está registrado en el evento')

            try:
                if user_lat and user_lng:
                    distance = calculate_distance(
                        event_location=event_location, user_location={"lat": user_lat, "lng": user_lng})
                    if distance <= 500:  # Si el usuario está a menos de 500 metros del evento
                        attend_attendance(attendance=attendance)
                        statusCode = 200
                        message = "Asistencia registrada correctamente"
                        print("Asistencia registrada. UserId:",
                              user_id, " EventId:", event_id)
                    else:
                        message = "El usuario no se encuentra en el lugar del evento."
                        print(
                            "Error asistencia. El usuario está a", distance, "metros del evento (límite 500m). UserId:", user_id, " EventId:", event_id)
                else:
                    message = "Asistencia registrada correctamente"
                    print(
                        "Se necesita la ubicación del usuario para tomar la asistencia")
            except Exception as e3:
                message = "Ocurrió un problema en el servidor. Intenta más tarde."
                print("Error marcando la asistencia", e3)
        except Exception as e2:
            message = "El evento o el usuario no existe. O el usuario no está registrado en el evento."
            print("Error trayendo objeto Attendance", e2)
    except Exception as e1:
        message = "El código QR es incorrecto."
        print("Error leyendo los datos de QR", e1)

    return {
        'statusCode': statusCode,
        'headers': {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': message
    }
