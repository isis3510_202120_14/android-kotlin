package com.example.androidkotlin.ui.tasks

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.adapters.TasksAdapter
import com.example.androidkotlin.databinding.FragmentTasksBinding
import java.util.ArrayList

class TasksFragment() : Fragment() {

  private lateinit var binding: FragmentTasksBinding
  private lateinit var viewModel: TasksViewModel

  private var tasksAdapter : TasksAdapter? = null
  lateinit var tasks: List<TaskUserView>

  private var editSheet: TaskSheetFragment? = null

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?,
  ): View? {
    binding = FragmentTasksBinding.inflate(inflater, container , false)
    viewModel=(activity as NavGraphActivity).tasksViewModel

    val activityContainer = requireActivity()

    // Recycler view
    viewModel.tasks.observe(viewLifecycleOwner, Observer{
      val tasksResult = it?: return@Observer

      if ( tasksResult.success != null ){
        tasks = tasksResult.success
        tasksAdapter = TasksAdapter(this, ArrayList<TaskUserView>(tasks))
        binding.taskRecycler.apply {
          layoutManager = LinearLayoutManager(context)
          setHasFixedSize(true)
          adapter = tasksAdapter
        }
      } else {
        Log.e("error","error")
      }
    } )

    // First tasks gif
    // Glide.with(activityContainer.applicationContext).load(R.drawable.first_note).into(binding.noDataImage)

    // Create task
    binding.addTask.setOnClickListener {
      val createTaskBottomSheetFragment = TaskSheetFragment()
      createTaskBottomSheetFragment.setTaskId("0", false, this)

      viewModel.createTask.observe(viewLifecycleOwner, Observer{
        val result = it?: return@Observer
        if (result.success != null){
          Toast.makeText(context, "Se ha añadido la tarea", Toast.LENGTH_SHORT).show()
          refresh()
          createTaskBottomSheetFragment.onCreatedTask()
        }
      })

      createTaskBottomSheetFragment.show(activityContainer.supportFragmentManager, createTaskBottomSheetFragment.tag)
    }

    // Delete task
    viewModel.deleteTask.observe(viewLifecycleOwner, Observer {
      val result = it?: return@Observer

      if (result.success != null){
        Toast.makeText(context, "Se ha eliminado la tarea", Toast.LENGTH_SHORT).show()
        refresh()
      }
    })


    // Calendar list
    /*binding.calendar.setOnClickListener {
      if(::tasks.isInitialized && tasks.isNotEmpty()){
        val showCalendarViewBottomSheet = ShowCalendarViewBottomSheet(tasks)
        showCalendarViewBottomSheet.show(activityContainer.supportFragmentManager, showCalendarViewBottomSheet.tag)
      }
    }*/

    viewModel.getTasks()
    return binding.root
  }

  fun refresh() {
    viewModel.getTasks()
  }

  fun getTask(id: String, sheet: TaskSheetFragment){
    editSheet = sheet
    viewModel.getTask(id)
  }

  fun createTask(task: TaskUserView){
    Log.i("Task debug", "creating")
    viewModel.createTask(task)
  }

  fun completeTask(id: String, task: TaskUserView) {
    viewModel.completeTask(id, task)
  }

  fun updateTask(id:String, task: TaskUserView){
    viewModel.updateTask(id, task)
  }

  fun deleteTask(id: String, position: Int){
    viewModel.deleteTask(id)
  }


}