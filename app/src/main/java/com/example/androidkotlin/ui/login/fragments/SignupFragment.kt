package com.example.androidkotlin.ui.login.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.androidkotlin.NavGraphActivity
import com.example.androidkotlin.R
import com.example.androidkotlin.databinding.FragmentSignupBinding
import com.example.androidkotlin.ui.login.LoginUserView
import com.example.androidkotlin.ui.login.LoginViewModel
import com.example.androidkotlin.utilities.CheckNetwork
import com.example.androidkotlin.utilities.DatePickerFragment
import com.example.androidkotlin.utilities.afterTextChanged
import com.google.android.material.textfield.TextInputEditText
import java.util.*


class SignupFragment : Fragment() {
    private lateinit var binding: FragmentSignupBinding
    private lateinit var loginViewModel: LoginViewModel

    private var birthdayDate: Date = Date()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentSignupBinding.inflate(inflater, container , false)
        loginViewModel = (activity as NavGraphActivity).loginViewModel
        loginViewModel.reset()

        val username = binding.txtEmail
        val fullname = binding.txtName
        val document = binding.txtDocument
        val phone = binding.txtPhone
        val birthday = binding.txtBirthday
        val city = binding.txtCity
        val password = binding.txtPassword
        val confirmPassword = binding.txtConfirmPassword
        val signup = binding.signupBtn
        val login = binding.loginTxt

        signup.isEnabled = false

        loginViewModel.signUpFormState.observe(viewLifecycleOwner, Observer {
            val signUpState = it ?: return@Observer

            // disable login button unless both username / password is valid
            signup.isEnabled = signUpState.isDataValid

            username.error = if (signUpState.usernameError != null) getString(signUpState.usernameError) else null
            fullname.error = if (signUpState.fullnameError != null) getString(signUpState.fullnameError) else null
            document.error = if (signUpState.documentError != null) getString(signUpState.documentError) else null
            phone.error = if (signUpState.phoneError != null) getString(signUpState.phoneError) else null
            birthday.error = if (signUpState.birthdayError != null) getString(signUpState.birthdayError) else null
            city.error = if (signUpState.cityError != null) getString(signUpState.cityError) else null
            password.error = if (signUpState.passwordError != null) getString(signUpState.passwordError) else null
            confirmPassword.error = if (signUpState.confirmPasswordError != null) getString(signUpState.confirmPasswordError) else null

        })

        loginViewModel.loginResult.observe(viewLifecycleOwner, Observer {
            val loginResult = it ?: return@Observer

            if (loginResult.error != null) {
                signup.isEnabled = loginViewModel.signUpFormState.value?.isDataValid ?: false
                showLoginFailed(loginResult.error, "Error al registrarse")
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
                (activity as NavGraphActivity).preferencesRepository.cleanSignupUser()
                binding.root.findNavController().navigate(SignupFragmentDirections.actionSignupFragmentToConfirmFragment(username.text.toString(), password.text.toString()))
            }
        })

        birthday.setOnClickListener{
            showDatePickerDialog(birthday)
        }

        val fields = listOf(username, fullname, document, phone, birthday, city, password, confirmPassword)
        fields.map {
            it.afterTextChanged {
                loginViewModel.signUpDataChanged(
                    username.text.toString(),
                    fullname.text.toString(),
                    document.text.toString(),
                    phone.text.toString(),
                    birthdayDate,
                    city.text.toString(),
                    password.text.toString(),
                    confirmPassword.text.toString()
                )
            }
        }

        signup.setOnClickListener {
            signup.isEnabled = false
            if ( CheckNetwork.isInternetAvailable((activity as NavGraphActivity).applicationContext) ){
                loginViewModel.signUp(username.text.toString(),
                    fullname.text.toString(),
                    document.text.toString(),
                    phone.text.toString(),
                    birthdayDate,
                    city.text.toString(),
                    password.text.toString())
            }else{
                showLoginFailed(R.string.error_check_connection, "Problemas de conexión")
                signup.isEnabled=true
            }
        }

        login.setOnClickListener {
            binding.root.findNavController().navigate(SignupFragmentDirections.actionSignupFragmentToLoginFragment())
        }

        // Initial date
        val calendarMin: Calendar = Calendar.getInstance()
        calendarMin.add(Calendar.YEAR, -19);
        birthdayDate = calendarMin.time

        // Data from shared preferences
        getSignupFormData((activity as NavGraphActivity).preferencesRepository.getSignupUser(), username, fullname, document, phone, birthday, city)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onStop() {
        val username = binding.txtEmail
        val fullname = binding.txtName
        val document = binding.txtDocument
        val phone = binding.txtPhone
        val city = binding.txtCity

        val temp = LoginUserView(
            email = username.text.toString(),
            fullname = fullname.text.toString(),
            document = document.text.toString(),
            phone = phone.text.toString(),
            birthday = birthdayDate,
            city = city.text.toString()
        )
        Log.i("Save", temp.toString())
        (activity as NavGraphActivity).preferencesRepository.saveSignupUser(temp)

        super.onStop()
    }

    private fun updateUiWithUser(model: LoginUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.email

        Toast.makeText(
            activity?.applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int, titulo: String) {
        binding.errorLayout.visibility = View.VISIBLE
        binding.errorTitle.text  = titulo
        binding.errorSubtitle.text = resources.getString(errorString)
    }


    private fun getSignupFormData(
        model: LoginUserView?,
        username: TextInputEditText,
        fullname: TextInputEditText,
        document: TextInputEditText,
        phone: TextInputEditText,
        birthday: TextInputEditText,
        city: TextInputEditText,
    ){
        if (model != null){
            username.setText(model.email)
            fullname.setText(model.fullname)
            document.setText(model.document)
            phone.setText(model.phone)
            if (model.birthday != null) birthdayDate = model.birthday
            city.setText(model.city)
        }
        val dateCal: Calendar = Calendar.getInstance()
        dateCal.time = birthdayDate

        val dateText = dateCal.get(Calendar.YEAR).toString() + " / " + (dateCal.get(Calendar.MONTH) + 1) + " / " + dateCal.get(Calendar.DAY_OF_MONTH)
        birthday.setText(dateText)
    }

    private fun showDatePickerDialog(birthday: TextInputEditText) {
        val dateCal: Calendar = Calendar.getInstance()
        dateCal.time = birthdayDate

        val newFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
            // +1 because January is zero
            val c = Calendar.getInstance()
            c.set(year, month, day, 0, 0)

            birthdayDate = c.time
            Log.i("Signup", birthdayDate.toString())
            val dateText = day.toString() + " / " + (month + 1) + " / " + year
            birthday.setText(dateText)
        }, dateCal.get(Calendar.YEAR), dateCal.get(Calendar.MONTH), dateCal.get(Calendar.DAY_OF_MONTH))

        newFragment.show(requireActivity().supportFragmentManager, "datePicker")
    }
}