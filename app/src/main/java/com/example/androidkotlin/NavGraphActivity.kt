package com.example.androidkotlin


import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.example.androidkotlin.data.PreferencesRepository
import com.example.androidkotlin.databinding.ActivityNavGraphBinding
import com.example.androidkotlin.ui.login.LoginViewModel
import com.example.androidkotlin.ui.login.LoginViewModelFactory
import android.content.Intent
import androidx.navigation.findNavController
import com.example.androidkotlin.ui.events.EventsViewModel
import com.example.androidkotlin.ui.events.EventsViewModelFactory
import com.example.androidkotlin.ui.tasks.TasksViewModel
import com.example.androidkotlin.ui.tasks.TasksViewModelFactory
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.example.androidkotlin.ui.profile.ProfileViewModel
import com.example.androidkotlin.ui.profile.ProfileViewModelFactory


class NavGraphActivity: AppCompatActivity() {
    // Variables
    private lateinit var sharedPreferences : PreferencesRepository
    private lateinit var binding: ActivityNavGraphBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    // ViewModels for all the fragments
    val loginViewModel: LoginViewModel by viewModels {
        LoginViewModelFactory((application as TechoApp).loginRepository)
    }
    val eventsViewModel: EventsViewModel by viewModels {
        EventsViewModelFactory((application as TechoApp).eventsRepository)
    }
    val tasksViewModel: TasksViewModel by viewModels {
        TasksViewModelFactory((application as TechoApp).tasksRepository)
    }

    val profileViewModel: ProfileViewModel by viewModels {
        ProfileViewModelFactory((application as TechoApp).loginRepository)
    }

    // Preferences repository
    val preferencesRepository by lazy { (application as TechoApp).preferencesRepository }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics
        binding = ActivityNavGraphBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Init Login and Preferences Repositories
        sharedPreferences = PreferencesRepository(applicationContext)
        if( sharedPreferences.getThemeMode() == PreferencesRepository.DARK_MODE){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else if ( sharedPreferences.getThemeMode() == PreferencesRepository.LIGHT_MODE){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
    }

    fun changeTheme(){
        when(AppCompatDelegate.getDefaultNightMode()){
            AppCompatDelegate.MODE_NIGHT_YES -> {
                sharedPreferences.saveThemeMode(PreferencesRepository.LIGHT_MODE)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
            AppCompatDelegate.MODE_NIGHT_NO -> {
                sharedPreferences.saveThemeMode(PreferencesRepository.DARK_MODE)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            }
        }
        val intent = Intent(this, NavGraphActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.nav_host_fragment)
        return navController.navigateUp()
    }
}