package com.example.androidkotlin.utilities

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    private var listener: DatePickerDialog.OnDateSetListener? = null

    private var initialYear: Int = -1
    private var initialMonth: Int = -1
    private var initialDay: Int = -1

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)

        if (initialYear == -1)
            initialYear = year - 18

        if (initialMonth == -1)
            initialMonth = c.get(Calendar.MONTH)

        if (initialDay == -1)
            initialDay = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(requireActivity(), listener, initialYear, initialMonth, initialDay)

        // Min and max date
        c.set(Calendar.YEAR, year - 18)
        datePickerDialog.datePicker.maxDate = c.timeInMillis
        return datePickerDialog
    }

    companion object {
        fun newInstance(listener: DatePickerDialog.OnDateSetListener): DatePickerFragment {
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }

        fun newInstance(listener: DatePickerDialog.OnDateSetListener, year: Int, month: Int, day: Int): DatePickerFragment {
            val fragment = DatePickerFragment()
            fragment.listener = listener
            fragment.initialYear = year
            fragment.initialMonth = month
            fragment.initialDay = day
            return fragment
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        TODO("Not yet implemented")
    }

}